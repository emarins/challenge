/*jshint esversion: 6 */
(function() {
  'use strict';

  angular.module('mainApp', [
    'ngRoute',
    'commonControllers',
    'commonServices'
  ])

  .config(($routeProvider) => {
    $routeProvider.when('/', {
      templateUrl: '../../index.html'
    });
  });
  
}(this, this.angular));
