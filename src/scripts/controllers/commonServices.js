/*jshint esversion: 6 */
(function() {
  'use strict';

  angular.module('commonServices', [])
    .factory('detailService', detailService);

  function detailService($http) {
    const service = {};
    const urlBase = 'https://www.gbm.com.mx/Mercados/ObtenerDatosGrafico?empresa=IPC';
    service.getData = () => {
      return $http.get(urlBase);
    };
    return service;
  }
})();