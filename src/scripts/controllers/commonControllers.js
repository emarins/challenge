/*jshint esversion: 6 */
(function() {
  'use strict';

angular.module('commonControllers', [])

.controller('MainController', ['$scope', 'detailService', ($scope, detailService) => {
  $scope.graphs = false;
  $scope.errorUser = false;
  $scope.loginSuccess = (name, pass) => {
    if (name === 'challenge' && pass === 'challenge') {
      detailService.getData()
        .then((response) => {
          $scope.drawGraph(response.data.resultObj);
          $scope.graphs = true;
        });
    } else {
      $scope.errorUser = true;
    }
  };

  $scope.drawGraph = (data) => {
    const margin = {top:20, bottom:20};
    const height = 400 - margin.top - margin.bottom;
    const width = window.innerWidth;

    const svg = d3.select('svg')
      .attr('width', width)
      .attr('height', height + margin.top + margin.bottom)
      .append('g')
      .attr('transform', 'translate(0,' + margin.top + ')')
      .datum(data);

    const parseTime = d3.timeParse('%Y-%m-%jT%H:%M:%S.%L%Z');

    const x = d3.scaleTime()
      .range([0, width]);

    const y = d3.scaleLinear()
      .range([height, 1]);

    d3.timeFormatDefaultLocale({decimal: ".",thousands: ",",grouping: [3],currency: ["$", ""],dateTime: "%A, %e de %B de %Y, %X",date: "%d/%m/%Y",time: "%H:%M:%S",periods: ["AM", "PM"],days: ["domingo", "lunes", "martes", "miércoles", "jueves", "viernes", "sábado"],shortDays: ["dom", "lun", "mar", "mié", "jue", "vie", "sáb"],months: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],shortMonths: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"]});

    const xAxis = d3.axisBottom()
      .ticks(4)
      .tickFormat(d3.timeFormat('%b %d (%H:%M)'))
      .scale(x);

    const yAxis = d3.axisLeft(y)
      .ticks(5)
      .tickPadding(30)
      .tickSize(0, -width);

    const area = d3.area()
      .x(function(d, i) { return x(parseTime(d.Fecha)); })
      .y1(function(d) { return y(d.Precio); })
      .y0(y(0))
      .curve(d3.curveMonotoneX);

    const line = d3.line()
      .x(function(d) { return x(parseTime(d.Fecha)); })
      .y(function(d) { return y(d.Precio); })
      .curve(d3.curveMonotoneX);

    /* Begin the iterate with data */
    x.domain(d3.extent(data, function(d) { return parseTime(d.Fecha); }));
    y.domain([d3.min(data, (d) => { return d.Precio; }), d3.max(data, function(d) { return d.Precio; })]);

    svg.append('path')
      .attr('class', 'area')
      .attr('d', area)
      .style('transform', 'translate(0, -300)')
      .style('opacity', '0')
      .transition()
        .delay(700)
        .duration(1000)
        .style('transform', 'translate(0, 0)')
        .style('opacity', '1');

    svg.append('path')
      .attr('class', 'line')
      .attr('d', line)
      .attr('stroke-dasharray', (width*10))
      .attr('stroke-dashoffset', (width*10))
      .transition()
        .duration(2000)
        .attr('stroke-dashoffset', 0);

    svg.append('g')
      .style('font-family', 'sans-serif')
      .attr('class', 'x axis')
      .attr('transform', 'translate(0, ' + height + ')')
      .call(xAxis);

    svg.append('g')
      .style('font-family', 'sans-serif')
      .attr('class', 'y axis')
      .attr('transform', 'translate(' + width + ',0)')
      .call(yAxis);
  };
}]);

})();
