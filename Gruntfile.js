/** Grunt config */
module.exports = (grunt) => {
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    clean: {
      build: ['tmp/', 'index.html']
    },
    connect: {
      server: {
        options: {
          port: 8888,
          livereload: 35729,
          hostname: 'localhost',
          path: 'www-root',
          options: {
            index: 'index.html',
          },
          open: true,
          reload: true,
        },
      },
    },
    jade: {
      compile: {
        files: {
          'index.html': 'src/templates/main.jade',
        },
        options: {
          pretty: true,
        },
      },
    },
    less: {
      development: {
        files: {
          'tmp/styles/main.css': 'src/styles/main.less',
        },
      },
    },
    jshint: {
      all: 'src/scripts/**',
    },
    wiredep: {
      task: {
        src: 'index.html',
      },
    },
    copy: {
      main: {
        files: [{
          expand: true,
          cwd: 'src/scripts',
          src: '**',
          dest: 'tmp/scripts/',
        }]
      },
    },
    watch: {
      livereload: {
        options: {livereload: true, spawn: true},
        tasks: ['clean', 'jade', 'less', 'jshint', 'wiredep', 'copy'],
        files: 'src/**/*',
        reload: true,
      },
    }
  });

  grunt.loadNpmTasks('grunt-contrib-clean');
  grunt.loadNpmTasks('grunt-contrib-jade');
  grunt.loadNpmTasks('grunt-contrib-less');
  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('grunt-wiredep');
  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-contrib-connect');
  grunt.loadNpmTasks('grunt-contrib-watch');

  grunt.registerTask('default', [
    'clean',
    'jade',
    'less',
    'jshint',
    'wiredep',
    'copy',
    'connect:server',
    'watch'
  ]);
};
